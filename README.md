# archive-work
Сервис работы с csv архивом

## Description
Данные берутся из архива на s3 сервере (настройка .secrets.yaml)
При обращении к архиву по уникальному имени
- Данные скачиваются в локальный кеш.
- Получаем список бакетов при отсутствии уникального номера
- Получаем архивов из бакета при отсутствии уникального номера
- Получаем список таблиц (файлов) при отсутствии номера (индекса) файла
- Отдаем данные файлов в пейжингом
- Делаем поиск по любой колонке
- Делаем процесс который чистит кеш через несколько дней после использования

## Installation

git clone git@gitlab.com:qqube/archive-service
cd archive-service
pipenv install --dev
pipenv shell

build/docker-compose up -d
## Usage

http://localhost:5000/swagger/


параметры page (по умолчанию=1) и page_size(50) не обязательны
bucket= уникальный идентификатор бакета
key=    уникальный идентифкатор архива
index=  порядковый номер файла в архиве

1. http://localhost:5000/archive/?key=3628019094_2024-03-01_17-42-05.zip&bucket=archive&index=0&page=1&page_size=3
Result:
[
    {
        "count": 738,
        "current": "http://localhost:5000/ceef807c/?index=0&page_num=1&page_size=3",
        "name": "pp_charges.csv",
        "next": "http://localhost:5000/ceef807c/?index=0&page_num=1&page_size=3",
        "page": 1,
        "page_max": 246,
        "page_size": 3,
        "previous": "",
        "results": [
            {
                "accounting_period_total": "352.6",
                "calc_value": "1116836396",
                "internal_id": "1116836396_012024_Водоотведение_ПУ_Холодное_водоснабжение_НЧ",
                "org_ppa_guid": "3628019094",
                "pp_internal_id": "1116836396_012024",
                "recalculation": "",
                "rr": "",
                "service_internal_id": "Водоотведение ПУ",
                "tariff": "35.26"
            },
            {
                "accounting_period_total": "197.36",
                "calc_value": "1116767921",
                "internal_id": "1116767921_012024_Холодное_водоснабжение_НЧ",
                "org_ppa_guid": "3628019094",
                "pp_internal_id": "1116767921_012024",
                "recalculation": "",
                "rr": "",
                "service_internal_id": "Холодное водоснабжение",
                "tariff": "49.34"
            },
            {
                "accounting_period_total": "419.39",
                "calc_value": "1116767939",
                "internal_id": "1116767939_012024_Холодное_водоснабжение_НЧ",
                "org_ppa_guid": "3628019094",
                "pp_internal_id": "1116767939_012024",
                "recalculation": "",
                "rr": "",
                "service_internal_id": "Холодное водоснабжение",
                "tariff": "49.34"
            }
        ]
    }
]

2. http://localhost:5000/archive/?key=3628019094_2024-03-01_17-42-05.zip&bucket=archive&page=1&page_size=3
параметр index= отсутствует, поэтому возвращаем список таблиц из арцива
Result:
[
    {
        "count": 738,
        "current": "http://localhost:5000/ceef807c/?index=0&page_num=1&page_size=3",
        "name": "pp_charges.csv"
    },
    {
        "count": 720,
        "current": "http://localhost:5000/ceef807c/?index=1&page_num=1&page_size=3",
        "name": "pu.csv"
    },
    {
        "count": 4,
        "current": "http://localhost:5000/ceef807c/?index=2&page_num=1&page_size=3",
        "name": "pp_service.csv"
    },
    {
        "count": 723,
        "current": "http://localhost:5000/ceef807c/?index=3&page_num=1&page_size=3",
        "name": "accounts.csv"
    },
    {
        "count": 17,
        "current": "http://localhost:5000/ceef807c/?index=4&page_num=1&page_size=3",
        "name": "pp.csv"
    },
    {
        "count": 707,
        "current": "http://localhost:5000/ceef807c/?index=5&page_num=1&page_size=3",
        "name": "puv.csv"
    }
]

3. archive/?key=3628019094_2024-03-01_17-42-05.zip&bucket=archive&index=0&page=1&page_size=3&service_internal_id=ПУ
Указываем фильтр по полю service_internal_id = ПУ. 
Фильтр работает по включению подстроки в строку. Числовые поля тоже рассматриваются как строковые
Result:
[
    {
        "count": 4,
        "current": "http://localhost:5000/ceef807c/?index=0&page_num=1&page_size=3",
        "name": "pp_charges.csv",
        "next": "http://localhost:5000/ceef807c/?index=0&page_num=1&page_size=3",
        "page": 1,
        "page_max": 2,
        "page_size": 3,
        "previous": "",
        "results": [
            {
                "accounting_period_total": "352.6",
                "calc_value": "1116836396",
                "internal_id": "1116836396_012024_Водоотведение_ПУ_Холодное_водоснабжение_НЧ",
                "org_ppa_guid": "3628019094",
                "pp_internal_id": "1116836396_012024",
                "recalculation": "",
                "rr": "",
                "service_internal_id": "Водоотведение ПУ",
                "tariff": "35.26"
            },
            {
                "accounting_period_total": "200.98",
                "calc_value": "1116767978",
                "internal_id": "1116767978_012024_Водоотведение_ПУ_Холодное_водоснабжение_НЧ",
                "org_ppa_guid": "3628019094",
                "pp_internal_id": "1116767978_012024",
                "recalculation": "",
                "rr": "",
                "service_internal_id": "Водоотведение ПУ",
                "tariff": "35.26"
            },
            {
                "accounting_period_total": "182.18",
                "calc_value": "1116767985",
                "internal_id": "1116767985_012024_Водоотведение_ПУ_Холодное_водоснабжение_НЧ",
                "org_ppa_guid": "3628019094",
                "pp_internal_id": "1116767985_012024",
                "recalculation": "",
                "rr": "",
                "service_internal_id": "Водоотведение ПУ",
                "tariff": "35.26"
            }
        ]
    }
]



## Support

## Roadmap

## Contributing


## Authors and acknowledgment

## License

## Project status
