import os, shutil
import hashlib
import pathlib
import zipfile
import logging
from src import app

logger = logging.getLogger(__name__)


def hashit(s):
    return hashlib.sha1(s.encode("utf-8")).hexdigest()[:8]


def lists_to_dict(headers, records):
    data = {}
    for index, value in enumerate(records.split(";")):
        data[headers.split(";")[index].strip()] = value.strip()
    return data


def get_extract_files(
    archive_file: pathlib.PosixPath, extract_dir: pathlib.PosixPath
) -> list:
    if not pathlib.Path.exists(archive_file):
        return []
    try:
        pathlib.Path.mkdir(extract_dir, exist_ok=True)
        names = []
        if archive_file.suffix == ".zip":
            with zipfile.ZipFile(archive_file, "r") as zip_file:
                names = [text_file.filename for text_file in zip_file.infolist()]
                for z in names:
                    zip_file.extract(z, extract_dir)
    except Exception as ex:
        logger.error(f"{ex}")
    finally:
        pathlib.Path.unlink(archive_file, missing_ok=True)
    return names


def get_cache_dirname(input_name: str, id: str) -> pathlib.PosixPath:
    pathlib.Path.mkdir(pathlib.Path(app.config.CACHE_DIR.NAME), exist_ok=True)
    return pathlib.Path(app.config.CACHE_DIR.NAME, hashit(input_name) if not id else id)


def get_filepaths(directory):
    """Возвращает список файлов из каталога и подкаталогов"""
    file_paths = []
    for root, _, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(os.path.basename(root), filename)
            file_paths.append(filepath)
    return file_paths


def get_folders(directory):
    """Возвращает список подкаталогов"""
    paths = []
    for file in os.listdir(directory):
        d = os.path.join(directory, file)
        if os.path.isdir(d):
            paths.append(d)
    return paths


def remove_folders(dirs: list):
    for path in dirs:
        if os.path.isdir(path):
            shutil.rmtree(path)


# import os
# import time

# path = '.idea'
# a = os.path.getatime(path)  # время последнего доступа
# b = os.path.getmtime(path)  # время последнего изменения
# c = os.path.getctime(path)  # время создания (Windows), время последнего изменения (Unix)
# print([time.ctime(x) for x in [a, b, c]])
