import pathlib, boto3, logging, os
from typing import Any, Optional
from src import app

logger = logging.getLogger(__name__)


class ServerS3:

    def __init__(self, **kwargs) -> None:
        self.s3 = boto3.resource(
            "s3",
            endpoint_url=app.config.S3.END_POINT,
            aws_access_key_id=app.config.S3.AWS_KEY,
            aws_secret_access_key=app.config.S3.AWS_SECRET,
        )
        self.bucket = (
            self.s3.Bucket(kwargs.get("bucket")) if kwargs.get("bucket") else None
        )
        self.parameters = dict()
        for key in kwargs:
            self.parameters[key] = kwargs.get(key)

    def fetch(self) -> Any:
        try:
            if self.bucket is None:
                # не задан бакет, то отправляем список бакетов
                return self.get_list_backets()
            elif self.parameters.get("key") is None:
                # не задан файл, то отправляем список файлов
                return self.get_list_files()
            else:
                return self.download_file()
        except Exception as ex:
            logger.error(f"{ex}")
        return []

    def download_file(self)->pathlib.PosixPath:
        output_name: pathlib.PosixPath = pathlib.Path(
            app.config.UPLOAD_DIR.NAME, self.parameters["key"]
        )
        self.bucket.download_file(self.parameters["key"], output_name)
        return output_name

    def get_list_backets(self):
        return [bucket.name for bucket in self.s3.buckets.all()]

    def get_list_files(self):
        list_files = list()
        for s3_object in self.bucket.objects.all():
            _, name = os.path.split(s3_object.key)
            list_files.append(name)
        if list_files and self.parameters.get("inn"):
            list_files = [x for x in list_files if self.parameters.get("inn") in x]
        return list_files
