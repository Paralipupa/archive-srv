import logging
import pathlib
from src.file_readers import get_file_reader


class Reader:
    def __init__(self, file_name: pathlib.PosixPath, **kwargs) -> None:
        ReaderClass = get_file_reader(file_name)
        self.reader = ReaderClass(file_name)
        self.param = dict()
        for key, value in kwargs.items():
            self.param[key] = value

    def all(self):
        return self.reader.all()

    def filter(self):
        return self.reader.filter(**self.param)
