import os
import pathlib
import logging, time
from datetime import datetime, timedelta
from dateutil.parser import parse
from typing import Tuple
from src import app
from src.helpers import (
    get_extract_files,
    get_cache_dirname,
    get_filepaths,
    get_folders,
    remove_folders,
)
from src.file_readers import get_file_reader
from src.pagginator import Paginator
from src.servers3 import ServerS3
from src.reader import Reader

logger = logging.getLogger(__name__)


class Cache:
    def __init__(self, **kwargs) -> None:
        self.parameters = dict()
        for key in kwargs:
            self.parameters[key] = kwargs[key]
        self.arch_filename = self.parameters.get("key")
        self.idx = self.parameters.get("index")
        self.cache_dirname = None
        if self.parameters.get("id") or self.arch_filename:
            self.cache_dirname = get_cache_dirname(
                self.arch_filename, self.parameters.get("id")
            )

    def fetch(self):
        """ Получить данные из архива s3 """
        names, is_cached = self.__get_files_or_list_backets_from_archive()
        if names and is_cached:
            return self.__get_data_from_csv(names)
        return names

    def clean(self):
        """" Очищаем кэш от старых папок """
        list_remove = []
        folders = get_folders(app.config.CACHE_DIR.NAME)
        days = int(self.parameters.get("days", "30"))
        deadline_date = datetime.today() - timedelta(days=days)
        list_remove = [
            x
            for x in folders
            if parse(time.ctime(os.path.getctime(folders[0]))) < deadline_date
        ]
        if list_remove:
            remove_folders(list_remove)
        return list_remove

    def __get_files_or_list_backets_from_archive(self) -> Tuple[list, bool]:
        if self.cache_dirname:
            if pathlib.Path.exists(self.cache_dirname):
                names = get_filepaths(self.cache_dirname)
                return names, True
        server = ServerS3(**self.parameters)
        result = server.fetch()
        if result:
            if not isinstance(result, list):
                names = get_extract_files(result, self.cache_dirname)
                names = get_filepaths(self.cache_dirname)
                return names, True
        return result, False

    def __get_data_from_csv(self, names: list) -> list:
        data_list = list()
        for index, name in enumerate(names):
            try:
                fname = pathlib.Path(self.cache_dirname, name)
                reader = Reader(fname, **self.parameters)
                data = reader.filter()
                paginator = self.__get_paginator(data, fname, index)
                if paginator:
                    data_list.append(paginator)
            except Exception as ex:
                logger.error(f"{ex}")
        return data_list

    def __get_paginator(self, data: list, fname: str, index: int) -> dict:
        if self.idx is None or index == int(self.idx):
            param_paginator = dict(
                id=self.parameters.get("id"),
                index=index,
                idx=self.idx,
                name=fname.name,
                host=self.parameters.get("host"),
                key=self.parameters.get("key"),
            )
            param_paginator["page"] = self.parameters.get("page")
            param_paginator["page_size"] = self.parameters.get("page_size")
            paginator = Paginator(data, **param_paginator)
            return paginator.page()
        return None
