import abc
import csv
import os
import logging
import pandas as pd
from typing import List
from src.helpers import lists_to_dict

logger = logging.getLogger(__name__)


def rchop(s, sub):
    return s[: -len(sub)] if s.endswith(sub) else s


class DataFile(abc.ABC):
    def __init__(self, fname):
        self._fname = fname
        self._sheet = None

    def __iter__(self):
        return self

    def __next__(self):
        return ""


class CsvFile(DataFile):
    def __init__(self, fname):
        super(CsvFile, self).__init__(fname)
        self.dt = pd.read_csv(
            fname, encoding="Windows-1251", delimiter=",", quotechar="|"
        )
        self.df = pd.DataFrame(data=self.dt)

    def all(self):
        headers = self.headers()
        data_list = self.items()
        final_list = []
        for lists in data_list:
            final_list.append(lists_to_dict(headers[0], lists[0]))
        return final_list

    def filter(self, **param):
        headers = self.headers()[0].split(";")
        keys = [x for x in param.keys()]
        fields = list(set(headers) & set(keys))
        data = self.all()
        if fields:
            try:
                for key in fields:
                    data = [x for x in data if param[key] in x[key]]
                return data
            except Exception as ex:
                logger.error(f"{ex}")
            return data
        else:
            return data

    def headers(self):
        return self.dt.columns.values.tolist()

    def items(self):
        return self.dt.values.tolist()


def get_file_reader(fname):
    """Get class for reading file as iterable"""
    _, file_extension = os.path.splitext(fname)
    if file_extension == ".csv":
        return CsvFile
    raise Exception("Unknown file type")
