import os
from flask import Flask
from dynaconf import FlaskDynaconf
from flasgger import Swagger

app = Flask(__name__, template_folder="../templates")
flask_conf = FlaskDynaconf(
    app,
    settings_files=["settings.yaml", ".secrets.yaml"],
)
swagger = Swagger(app)
app.config.CACHE_DIR.NAME = os.environ.get("CACHE_DIR_NAME", app.config.CACHE_DIR.NAME)
from src import routes
from src.settings import *
