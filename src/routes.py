import logging
from flasgger import swag_from
from flask import (
    request,
    jsonify,
    make_response,
)
from flask_api import status
from flask_restful import abort
from src import app, api
from src.helpers import hashit
from src.auth import multi_auth
from src.cache import Cache

logger = logging.getLogger(__name__)


@app.route("/archive/", defaults={"id": None})
@app.route("/<string:id>/", endpoint="archive_with_id", methods=["GET"])
# @multi_auth.login_required()
@swag_from("../docs/archive.yml", endpoint="archive_with_id")
def fetch_list_of_files(id):
    """получить список файлов по идентификатору архива с серверв s3
    Result:
    1.Списко бакетов (если не задан парамет backet=)
    2.Список объектов в бакете (если не задан парамет key=)
    3.Список файлов из объекта в бакете (если не задан парамет index=)
    4.Данные файла
    """
    param = __get_param(id=id)
    result = api.fetch_list_files(**param)
    try:
        return jsonify(result)
    except Exception as ex:
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR, **dict(message=f"{ex}"))

@app.route("/cleancache/", methods=["GET"])
# @multi_auth.login_required()
@swag_from("../docs/clean.yml", endpoint="clean_cache")
def clean_cache():
    """Почистить папки из кэша ранее 30 дней по-умолчанию
    параметр days=количество дней для хранения папок в кэше"""
    param = __get_param()
    result = api.clean_cache(**param)
    try:
        return jsonify(result)
    except Exception as ex:
        abort(status.HTTP_500_INTERNAL_SERVER_ERROR, **dict(message=f"{ex}"))


### Обработка ошибок запросов ###################################################
def get_error_response(error, code, message, **kwargs):
    error_message = (
        (error.data.get("message") if hasattr(error, "data") else None)
        or kwargs.get("message")
        or message
    )
    logger.error("%s: error: %s ", code, error_message)
    response = make_response(
        {
            "result": "error",
            "error": error_message,
        },
        code,
    )
    response.headers["Content-Type"] = "application/json; charset=utf-8"
    return response


@app.errorhandler(status.HTTP_404_NOT_FOUND)
def not_found(error, **kwargs):
    return get_error_response(
        error, status.HTTP_404_NOT_FOUND, "404 Not found", **kwargs
    )


@app.errorhandler(status.HTTP_400_BAD_REQUEST)
def bad_request(error, **kwargs):
    return get_error_response(
        error, status.HTTP_400_BAD_REQUEST, "400 Bad Request", **kwargs
    )


@app.errorhandler(status.HTTP_401_UNAUTHORIZED)
def unauthorized(error, **kwargs):
    return get_error_response(
        error, status.HTTP_401_UNAUTHORIZED, "401 Unauthorized", **kwargs
    )


@app.errorhandler(status.HTTP_500_INTERNAL_SERVER_ERROR)
def int_error(error, **kwargs):
    return get_error_response(
        error, status.HTTP_500_INTERNAL_SERVER_ERROR, "500 Internal Error", **kwargs
    )


#################################################################################################


def __get_param(id=None, attach=None):
    param = {}
    for key, value in request.args.items():
        param[key] = value
    param["host"] = request.host_url
    if id:
        param["id"] = id
    elif param.get("key"):
        param["id"] = hashit(param["key"])
    return param
