from src import app


class Paginator:

    def __init__(self, data, **kwargs):
        self.data = data
        self.id = kwargs.get("id")
        self.index = kwargs.get("index")
        self.idx = kwargs.get("idx")  # индекс, переданый параметров в url
        self.name = kwargs.get("name")
        self.page_num = kwargs.get("page")
        self.page_size = kwargs.get("page_size")
        self.host = kwargs.get("host")
        self.key = kwargs.get("key")

    def page(self) -> dict:
        paginat = {}
        if self.name:
            paginat["name"] = self.name
        paginat["count"] = self.count()
        paginat["current"] = self.link(self.current_page())
        if not self.idx is None or self.idx == "":
            paginat["page"] = self.current_page()
            paginat["page_size"] = self.size_page()
            paginat["page_max"] = self.max_pages()
            paginat["previous"] = self.link(paginat["page"] - 1)
            paginat["next"] = self.link(paginat["page"] + 1)
            paginat["results"] = self.data[
                (paginat["page"] - 1)
                * paginat["page_size"] : paginat["page"]
                * paginat["page_size"]
            ]
        return paginat

    def count(self) -> int:
        return len(self.data)

    def max_pages(self) -> int:
        return (self.count() // self.size_page()) + (
            1 if self.count() % self.size_page() != 0 else 0
        )

    def link(self, page_num: int) -> str:
        if 1 <= page_num <= self.max_pages():
            parameters = ["index", "page_num", "page_size"]
            path = ""
            for p in parameters:
                path += (
                    f"&{p}={self.__dict__.get(p)}"
                    if not self.__dict__.get(p) is None
                    else ""
                )
            path = path.strip("&")
            url = self.host
            if not self.id is None:
                url += f"{self.id}/"
            if path:
                url += f"?{path}"
        else:
            url = ""
        return url

    def current_page(self) -> int:
        try:
            return (
                int(self.page_num)
                if not self.page_num is None and str(self.page_num).isdigit()
                else 1
            )
        except Exception as ex:
            return 1

    def size_page(self) -> int:
        try:
            size = (
                int(self.page_size)
                if not self.page_size is None and str(self.page_size).isdigit()
                else app.config.PAGINATOR.PageSize
            )
            if size <= 0 or size > app.config.PAGINATOR.PageSize:
                size = app.config.PAGINATOR.PageSize
            return size
        except Exception as ex:
            return app.config.PAGINATOR.PageSize
