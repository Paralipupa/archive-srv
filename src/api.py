import os, uuid
import logging
from botocore.config import Config
from functools import lru_cache
from flask_api import status
from src import app
from src.cache import Cache
from src.exceptions import *

logger = logging.getLogger(__name__)

def fetch_list_files(**param):
    """Получить данные из кеша по идентификатору архива"""
    try:
        cache = Cache(**param)
        names = cache.fetch()
        return names
    except ConnectionErrorException as ex:
        return f"{ex}"
    except Exception as ex:
        return f"{ex}"

def clean_cache(**param):
    """Очистка кэша"""
    try:
        cache = Cache(**param)
        names = cache.clean()
        return names
    except ConnectionErrorException as ex:
        return f"{ex}"
    except Exception as ex:
        return f"{ex}"

if __name__ == "__main__":
    pass
