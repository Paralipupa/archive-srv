#!/bin/sh

NUM_WORKERS=3
TIMEOUT=600
gunicorn --bind 0.0.0.0:5000 main:app \
--workers $NUM_WORKERS \
--timeout $TIMEOUT
